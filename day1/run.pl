use File::Slurp;
use Modern::Perl;


sub open_file_to_lines {
  my $input = read_file("./input.txt");
  my @lines = split "\n", $input;
  return @lines;
}

sub part1 {
  my @lines = open_file_to_lines(shift @_);
  my $increase_count = 0;
  for (my $i = 0; $i < $#lines; $i++) {
    my $current = $lines[$i];
    my $next = $lines[$i+1];

    if ($next > $current) {
      $increase_count++;
    }
  }
  say "There have been $increase_count increases";
}

sub part2() {
  my @lines = open_file_to_lines(shift @_);
  my $increase_count = 0;

  my $old_ass_value = 100000000000; #dummy number to skip the first iteration
  for (my $i = 0; $i < $#lines -1; $i++) {
    my $current = $lines[$i];
    my $next = $lines[$i+1];
    my $nextnext = $lines[$i+2];

    my $sum = $current + $next + $nextnext;

    if ( $sum > $old_ass_value) {
      $increase_count++;
    }

    $old_ass_value = $sum;
  }
  say "There have been $increase_count increases";
}

part1();
part2();
