use File::Slurp;
use Modern::Perl;

require "./Sub.pm";

sub open_file_to_lines {
  my $input = read_file(shift @_);
  my @lines = split "\n", $input;
  return @lines;
}

sub parse_line {
  my $line = shift;
  my ($direction, $amount) = $line =~ m/(.*?) (.*)/;
  return $direction, $amount;
}

sub part1 {
  my @lines = open_file_to_lines(shift @_);
  my $sub =  Sub->new(depth=>0, horizontal=>0);

  foreach my $line (@lines) {
    my ($direction, $amount) = parse_line($line);
    $sub->move($direction, $amount);
  }

  say "Final distance is " . $sub->distance();
}

sub part2 {
  my @lines = open_file_to_lines(shift @_);
  my $sub =  Sub->new(depth=>0, horizontal=>0);

  foreach my $line (@lines) {
    my ($direction, $amount) = parse_line($line);
    $sub->move_with_aim($direction, $amount);
  }

  say "Final distance is " . $sub->distance();
}

part1("input.txt");
part2("input.txt");
