package Sub;

use Moo;
use Modern::Perl;

has depth => ( is => 'rw', default => 0 );
has horizontal => ( is => 'rw', default => 0 );
has aim => ( is => 'rw', default => 0 );

sub distance  {
     my $self = shift;
     return $self->depth * $self->horizontal;
}

sub move {
  my ($self, $direction, $amount) = @_;
  if ($direction eq "forward") {
    $self->horizontal($self->horizontal + $amount);
  }

  if ($direction eq "down") {
    $self->depth($self->depth + $amount);
  }
  if ($direction eq "up") {
    $self->depth($self->depth - $amount);
  }
}

sub move_with_aim {
  my ($self, $direction, $amount) = @_;
  if ($direction eq "forward") {
    $self->horizontal($self->horizontal + $amount);
    $self->depth($self->depth + ($self->aim * $amount))
  }

  if ($direction eq "down") {
    $self->aim($self->aim + $amount);
  }
  if ($direction eq "up") {
    $self->aim($self->aim - $amount);
  }
}


1;
